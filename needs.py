#!/usr/bin/env python
# encoding: utf-8

"""needs - show build dependencies in a directed graph

Usage:
    needs [options] <dotfile> ...
    needs -h | --help

Options:
    -h, --help             show this screen
    -d, --debug            enable debug output

    -s, --show             show resulting graph interactively (using xdot)
    -o, --output FILE      write resulting dot file
    -f, --filter MODULE    filter graph for (in)direct dependencies of MODULE
    --nojoin               don't join module configurations, e.g. IncludeOnly / Lib
"""

import contextlib
import docopt
import logging
import networkx as nx
import os
import pydot
import subprocess
import sys
import tempfile

from networkx.drawing import nx_pydot

@contextlib.contextmanager
def TemporaryDotFile(mode="w+b"):
    tmp = tempfile.NamedTemporaryFile(mode=mode, suffix=".dot", delete=False)
    tmp.close()
    yield tmp.name
    os.unlink(tmp.name)


def read_dot(dotfile):
    logging.info("read graph from {}".format(dotfile))
    try:
        graph = nx_pydot.read_dot(dotfile)
    except FileNotFoundError:
        logging.error("error reading dot file {}".format(dotfile))
        sys.exit(1)
    return graph


def write_dot(g, path):
    logging.info("writing dotfile {}".format(path))
    try:
        nx_pydot.write_dot(g, path)
    except:
        logging.error("cannot write to {}".format(path))
        sys.exit(1)


def show_dot(dotfile):
    try:
        logging.info("show {}".format(dotfile))
        process_result = subprocess.run(["xdot", dotfile],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
    except subprocess.CalledProcessError:
        logging.error("cmd xdot failed with status {}\n{}".format(
            process_result.returncode, process_result.stderr.decode("utf-8")))


def show_graph(graph):
    with TemporaryDotFile() as tmp_dot:
        write_dot(graph, tmp_dot)
        show_dot(tmp_dot)


def join_module_configs(graph):
    node_mapping = dict()
    for node, tokens in [(node, node.split(",")) for node in graph.nodes if "," in node]:
        node_mapping[node] = tokens[0]
        logging.debug("mapping {} -> {}".format(node, tokens[0]))
    nx.relabel_nodes(graph, node_mapping, copy=False)


def remove_selfloops(graph):
    for edge in list(graph.selfloop_edges()):
        logging.debug("remove selfloop {} -> {}".format(*edge))
        graph.remove_edge(*edge)


def strip_common_prefix(graph):
    commonprefix = os.path.commonprefix(list(graph.nodes))
    logging.debug("stripping common prefix '{}'".format(commonprefix))

    node_mapping = dict()
    for node in graph.nodes:
        node_mapping[node] = os.path.relpath(node, commonprefix).replace("\\","/")
    nx.relabel_nodes(graph, node_mapping, copy=False)


def transitive_reduction(graph):
    if nx.algorithms.dag.is_directed_acyclic_graph(graph):
        logging.debug("using networkx transitive_reduction algorithm")
        graph = nx.algorithms.dag.transitive_reduction(graph)
    else:
        logging.warning("graph has cycles, transitive reduction is not unqiue")
        for cycle in nx.algorithms.cycles.simple_cycles(graph):
            logging.warning("  cycle {}".format(cycle))

        with TemporaryDotFile() as tmp_dot:
            logging.warning("use external program tred for reduction")
            write_dot(graph, tmp_dot)
            try:
                process_result = subprocess.run(["tred", tmp_dot, " > ", tmp_dot],
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE)
            except subprocess.CalledProcessError:
                logging.error("tred exited with status {}\n{}".format(
                    process_result.returncode, process_result.stderr.decode("utf-8")))
                sys.exit(1)
            graph = read_dot(tmp_dot)

    return graph


def load_graphs(dotfiles, join_configs=True):
    graphs = list()

    for graph in [read_dot(dotfile) for dotfile in dotfiles]:
        if join_configs:
            join_module_configs(graph)
        graphs.append(graph)

    return nx.algorithms.operators.all.compose_all(graphs)


def filter(graph, modules):
    nodes_to_keep = set()

    for node in graph.nodes:
        for module in [m for m in modules if m in node]:
            nodes_to_keep.add(node)
            nodes_to_keep.update(nx.algorithms.dag.ancestors(graph, node))
            nodes_to_keep.update(nx.algorithms.dag.descendants(graph, node))
    logging.debug("nodes_to_keep: {}".format(nodes_to_keep))

    for node in [n for n in graph.nodes if n not in nodes_to_keep]:
        logging.debug("removing unrelated node {}".format(node))
        graph.remove_node(node)


if __name__ == '__main__':
    args = docopt.docopt(__doc__)

    logLevel = logging.DEBUG if args.get("--debug", False) else logging.INFO
    logging.basicConfig(format="[%(levelname)7s]: %(message)s", level=logLevel)

    logging.debug("args were\n{}".format(args))

    graph = load_graphs(args["<dotfile>"], join_configs=not args["--nojoin"])

    remove_selfloops(graph)
    strip_common_prefix(graph)

    if args["--filter"]:
        filter(graph, args["--filter"].split(","))

    graph = transitive_reduction(graph)

    if args["--output"]:
        write_dot(graph, args["--output"])

    if args["--show"]:
        show_graph(graph)
