needs - Build dependency explorer
=================================

**needs** is a small python tool to explore build dependencies. It learns about
inter-module dependencies by reading `dot`-files containing the relations
between modules in a simple format, i.e. `moduleA -> moduleB`. Those
`dot`-files may be generated by the build system, i.e. bake_ (and possibly
`cmake`)


Dependencies
------------

**needs** itself is developed on **Python3.6** and requires the modules listed
in `requirements.txt`. You can easily install those dependencies via

.. code-block:: console

    pip install -r requirements.txt

Further you will need `graphviz` to be installed and `xdot` for an interactive
presentation. If you're on a decent Linux distribution, install them via your
package manager, e.g.

.. code-block:: console

    sudo apt-get install graphviz xdot


Usage
-----

Starting with **bake** version 2.54.0 it is easy to bulk-generate the `dot`
dependency files using `bakery` with the `--dot` option e.g.

.. code-block:: console

    bakery UnitTestBase --adapt gcc --dot

This generates a `UnitTestBase.dot` file in every modules root folder containing
its dependencies. **needs** can merge them for you via

.. code-block:: console

    find ~/bsw/central -name UnitTestBase.dot | xargs ./needs.py --nojoin -o all.dot

To explore the build dependencies for your module of interest, run **needs** like

.. code-block:: console

    ./needs.py --show --filter=lwipSocket2 all.dot


.. _bake:: https://github.com/esrlabs/bake
